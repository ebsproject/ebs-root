#!/bin/bash

### This script provides replaces the host variable in src/index.ejs to specified host.
###     
###     Run command: bash update_host.sh ${HOST}
###     example: bash update_host.sh https://sample.org
### 

### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

### Escape special character /
HOST=$1
HOST=$(sed 's/\//\\\//g' <<< $HOST);

HOST_URL="${HOST}\/mfe\/sharedlib\/dev\/importmaps.json"

HOST_STR="<script type=\"systemjs-importmap\" src"
FILE="src/index.ejs"

if [ $# -eq 0 ]; then
    echo;
    echo "$RED ☒  Invalid command. Please provide host. $RESET"
    exit;
fi

while read -r line; do

    if [[ "$line" == "${HOST_STR}"* ]] ;then
        if sed -i -e "s/${HOST_STR}=.*/${HOST_STR}=\"${HOST_URL}\"><\/script>/" $FILE; then
            echo;
            echo "$GREEN ☒  Host successfully updated to $HOST_URL. $RESET"
            exit;
        else 
            echo;
            echo "$RED ☒  Update of host failed. Kindly check for unescaped special characters. $RESET"
            exit;
        fi
    fi

done < "$FILE"